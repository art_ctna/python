# I. Tipo de problema:
# 1. Detección de outliers
# 2. Forecasting
# 3. Sistema de recomendación
# 4. Clasificación
# 4.1 Una clase
# 4.2 Binaria
# 4.3 Multiclase
# 4.A Métodos paramétricos
# 4.B Métodos no paramétricos
# 5. Regresión
# 6. Agrupamiento/clustering
# 6.1 Número de clases conocidas
# 6.2 Número de clases deconocidas
# 6.A Agrupamiento jerarquico
# 6.B Agrupamiento aglomerativo
# - ¿Se tienen suficientes datos?
# -- Curvas de error vs tamaño de muestra
# - ¿Se requiere un análisis del límite de desempeño teórico?
# -- Análisis de error de bayes

# II. Construcción de conjuntos de validación
# 1. Entrenamiento y prueba
# 2. Bootstrap
# 3. K-fold cross validation. Validación cruzada de k pliegues.
# A. Estratificado
# B. No estratificado

# III. Normalización de atributos
# 1. Sin normalización
# 2. Min-Max
# 3. Soft-max
# 4. Standard normalization. Z-score

# IV. Imputación de atributos
# 1. Imputación simple.
# 2. Imputación predictiva. MICE.

# V. Selección de características y reducción de dimensionalidad.
# 1. Método supervisado. Análisis discriminante de Fisher.
# 2. Método no supervisado. Análisis de componentes principales.
# A. Métodos filtro.
# B. Métodos envoltorio.
# C. Métodos filtro-envoltorio.
# D. Métodos embebidos.

# VI. Generación de datos sintéticos
# 1. SMOTE

# VII. Métrica de evaluación de modelos
# 1. Accuracy
# 2. Area under ROC Curve
# 3. Sensitivity, specificity, precision or postive predictive value,  negative predictive value
# 4. F1
# 5. MCC. Matthews correlation coefficient

# VIII. Búsqueda de parámetros. Evaluación y selección del modelo.
# 1 Búsqueda malla
# 2 Búsqueda con metaheurísticas

# python DevelopmentAtom.py n3 i1 cw1 c0
# python DevelopmentAtom.py n3 i1 cw1 c5

import numpy as np
import pandas as pd
import time
import sys

class DevelopmentAtom():
    def TrainEval(self, opcn="n3", opci="i1", cw="cw1", clc="c0", classifier_file=None, test=None):
        start = time.time()
        rcols = ["status_id","created_at","updated_at"]
        if type(test) is np.ndarray:
            testing = True
        else:
            testing = False

        if testing == True and classifier_file != None:
            import pickle
            try:
                output = pickle.load(open( classifier_file, "rb" ))
                classifier = output["classifier"]
                model_options = output["model_options"]
                opcn = model_options["opcn"]
                opci = model_options["opci"]
                cw = model_options["cw"]
                clc = model_options["clc"]
                mmscaler = output["mmscaler"]
                sc = output["sc"]
                sc_cols = output["sc_cols"]
                m = output["m"]
                rcols_id = output["rcols_id"]
            except Exception as e:
                print(e)
                print("Error loading classifier model")
                return None

        if testing == False:
            #Carga de información
            try:
                basename = "base_df_000"
                df = pd.read_pickle(basename + ".pkl")
                X = df.drop(rcols, axis=1).values
                y = (df["status_id"].values==9).astype(int)
                [n,m] = X.shape
                rcols_id = [df.columns.get_loc(col) for col in rcols]
            except Exception as e:
                print(e)
                print("Error loading training data")
                return None

        if testing == True:
            [nt, mt] = test.shape
            if mt == m+len(rcols):
                test = np.delete(test, rcols_id)
                test = test.astype(float)
                test = test.reshape(1,-1)
                [nt, mt] = test.shape
            else:
                print("Error loading test data")
                return None

        # II. Construcción de conjuntos de validación
        from sklearn.model_selection import train_test_split
        if testing == False:
            X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, test_size=0.4, random_state=42)

        # III. Normalización de atributos
        from sklearn.preprocessing import StandardScaler, MinMaxScaler
        if testing == False:
            sc = StandardScaler()
            mmscaler = MinMaxScaler()
            sc_cols = []

        if (opcn == "n0" and testing == False):
            print("-- Without scaling")

        if (opcn == "n1" and testing == False):
            print("-- Standar scaler")
            X_train = sc.fit_transform(X_train)
            X_test = sc.transform(X_test)
            sc.fit(X)
        elif (opcn == "n1" and testing == True):
            test = sc.transform(test)

        if (opcn == "n2" and testing == False):
            print("-- MinMax scaler")
            X_train = mmscaler.fit_transform(X_train)
            X_test = mmscaler.transform(X_test)
            mmscaler.fit(X)
        elif (opcn == "n2" and testing == True):
            test = mmscaler.transform(test)

        if (opcn == "n3" and testing == False):
            print("-- Standar scaler and MinMax scaler")
            X_train = sc.fit_transform(X_train)
            X_test = sc.transform(X_test)
            X2_train = mmscaler.fit_transform(X_train)
            X2_test = mmscaler.transform(X_test)

            for i in range(0,m):
                if (len(np.unique(X[:,i])) < 4):
                    sc_cols.append(i)
                    X_train[:,i] = X2_train[:,i]
                    X_test[:,i] = X2_test[:,i]
            sc.fit(X)
            mmscaler.fit(X)
        elif (opcn == "n3" and testing == True):
            test = sc.transform(test)
            test2 = mmscaler.transform(test)
            for i in sc_cols:
                test[:,i] = test2[:,i]

        # IV. Imputación de atributos
        from sklearn.impute import SimpleImputer
        if (opci == "i0" and testing == False):
            print("-- No imputation")
            imp = None

        if (opci == "i1" and testing == False):
            print("-- Simple imputation")
            imp = SimpleImputer(missing_values=np.nan, strategy='median')
            imp.fit(X_train)
            X_test = imp.transform(X_test)
        elif (opcn == "i1" and testing == True):
            test = imp.transform(test)

        # VIII. Parámetros de modelo
        if cw == "cw0" and testing == False:
            print("-- Balanced: None")
            balanced = None

        if cw == "cw1" and testing == False:
            print("-- Balanced: True")
            balanced = "balanced"

        # VIII. Método de evaluación de modelo
        from sklearn.ensemble import RandomForestClassifier
        if (clc == "c0" and testing == False):
            print("-- -- Random Forest")
            classifier = RandomForestClassifier(n_estimators=100, random_state=0, max_depth=None, min_samples_split=2, min_samples_leaf=1, criterion="gini", class_weight=balanced)
            classifier.fit(X_train, y_train)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y)
        elif (clc == "c0" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        from sklearn.svm import SVC
        if (clc == "c1" and testing == False):
            print("-- -- SVC linear")
            classifier = SVC(kernel='rbf', probability=True, class_weight=balanced)
            classifier.fit(X_train, y_train)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y)
        elif (clc == "c1" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        from sklearn.svm import SVC
        if (clc == "c2" and testing == False):
            print("-- -- SVC auto gaussian")
            classifier = SVC(kernel='rbf', probability=True, gamma='scale', shrinking=True, class_weight=balanced)
            classifier.fit(X_train, y_train)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y)
        elif (clc == "c2" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        from sklearn.svm import SVC
        if (clc == "c3" and testing == False):
            print("-- -- SVC 0.5 gaussian")
            classifier = SVC(kernel='rbf', probability=True, gamma=0.5, shrinking=True, class_weight=balanced)
            classifier.fit(X_train, y_train)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y)
        elif (clc == "c3" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.tree import DecisionTreeClassifier
        if (clc == "c4" and testing == False):
            print("-- -- Adaboost")
            classifier = AdaBoostClassifier(DecisionTreeClassifier(random_state=0, max_depth=None, min_samples_split=2, min_samples_leaf=1, criterion="gini", class_weight=balanced),
                                 algorithm="SAMME.R",
                                 n_estimators=100)
            classifier.fit(X_train, y_train)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y)
        elif (clc == "c4" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        from sklearn.naive_bayes import GaussianNB
        from sklearn.utils.class_weight import compute_class_weight
        if (clc == "c5" and testing == False):
            classifier = GaussianNB()
            print("-- -- Gaussian NB")
            if balanced=="balanced":
                class_weights = compute_class_weight('balanced', y_train, y_train)
                class_weights_f = compute_class_weight('balanced', y, y)
            else:
                class_weights = np.ones(y_train.shape)
                class_weights_f = np.ones(y.shape)
            classifier.fit(X_train, y_train, class_weights)
            y_pred = classifier.predict(X_test)
            y_pred_probs =  classifier.predict_proba(X_test)
            classifier.fit(X, y, class_weights_f)
        elif (clc == "c5" and testing == True):
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)

        # VII. Métrica de evaluación de modelos
        from sklearn.metrics import classification_report, confusion_matrix, \
        accuracy_score, f1_score, roc_curve, auc, precision_score, recall_score
        if testing == False:
            end = time.time()
            runtime = end - start
            cm = confusion_matrix(y_test,y_pred)
            cr = classification_report(y_test,y_pred)
            fpr, tpr, thresholds = roc_curve(y_test, y_pred_probs[:,1], pos_label=1)
            auc_score = auc(fpr, tpr)
            f1_score = f1_score(y_test,y_pred,pos_label=1)
            acc_score = accuracy_score(y_test, y_pred)
            pre_score = precision_score(y_test, y_pred)
            rec_score = recall_score(y_test, y_pred)
            pos = np.sum(y == 1)
            neg = np.sum(y == 0)
            print("-- Resultados")
            print("Confusion matrix")
            print(cm)
            print("Classification report")
            print(cr)
            print("AUC score: "+ "{:.2f}".format(auc_score))
            print("F1: " + "{:.2f}".format(f1_score))
            print("Acc: " + "{:.2f}".format(acc_score))
            print("Precision: " + "{:.2f}".format(pre_score))
            print("Recall: " + "{:.2f}".format(rec_score))
            print("Positive: " + str(pos))
            print("Negative: " + str(neg))
            print("Runtime: " + "{:.3f}".format(runtime/60) + " minutes")
            model_options = {}
            model_options["opcn"] = opcn
            model_options["opci"] = opci
            model_options["cw"] = cw
            model_options["clc"] = clc
            model_options["classifier_file"] = classifier_file
            model_options["test"] = test
            output = {}
            output["classifier"] = classifier
            output["model_options"] = model_options
            output["cm"] = cm
            output["cr"] = cr
            output["auc_score"] = auc_score
            output["f1_score"] = f1_score
            output["acc_score"] = acc_score
            output["pre_score"] = pre_score
            output["rec_score"] = rec_score
            output["pos"] = pos
            output["neg"] = neg
            output["mmscaler"] = mmscaler
            output["sc"] = sc
            output["sc_cols"] = sc_cols
            output["m"] = m
            output["rcols_id"] = rcols_id
            output["runtime"] = runtime
            import pickle
            if classifier_file != None:
                pickle.dump( output, open( classifier_file, "wb" ) )
            return output

        if testing == True:
            y_pred = classifier.predict(test)
            y_pred_probs =  classifier.predict_proba(test)
            print("Clase:" + str(y_pred))
            print("Probabilidad: " + str(y_pred_probs))
            output = {}
            output["y_pred"] = y_pred
            output["y_pred_probs"] = y_pred_probs
            return output

        return None

if __name__ == "__main__":
    print ('Number of arguments:' + str(len(sys.argv)) + ' arguments.')
    print ('Argument List:' + str(sys.argv))

    opcn = "n0" # Normalización
    opci = "i0" # Imputación
    clc = "c0" # Clasificador
    cw = "cw0" # Balanceo de clases

    test_instance = False
    classifier_file = "model_0000.pkl"
    test = False
    if test == True:
        test = np.array([29.0, 71, 29, 0, 0, 0, 27.0, 0, 0, 0, 0, 0, \
        '2019-09-09 15:51:19.148000+0000', 0, 0, \
        -45.0, 0, 0, 0, 38, 1, 1, 0, 0, 0, 0, -800, 0, 2, 38, 0, 1, 0, 0, \
        27000.0, 8000.0, 1.47058823529412, 2.0, 1, 2, -1, 9, 0, 60.0, 15.0, \
        1.33333333333333, \
        '2019-09-23 22:48:22.978355+0000', 0, 0]).reshape(1, -1)
        print(test.shape)
    if len(sys.argv) == 5:
        opcn = sys.argv[1]
        opci = sys.argv[2]
        cw = sys.argv[3]
        clc = sys.argv[4]

    print("opciones de normalización modelo: "+ str(opcn))
    print("opciones de imputación: "+ str(opci))
    print("clasificador: "+ str(clc))
    print("balance de clases: "+ str(cw))
    print("-- Opciones del modelo")
    dvp = DevelopmentAtom()
    dvp.TrainEval(opcn, opci, cw, clc, classifier_file, test)
