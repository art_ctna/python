from math import sqrt
import warnings
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
import sys,importlib
import reg as r



df = pd.read_csv('kc_house_data.csv')
y = df["price"].to_numpy()
#X = df.drop(columns = ['id','date','price']).to_numpy()
#X = np.squeeze(np.array(df[["sqft_basement", "yr_built"]]))
X = df["yr_built"].to_numpy()


importlib.reload(sys.modules["reg"])
rmse = r.evaluate_reg_model(X, y, 2, 0.3)
