import numpy as np
from math import sqrt
import warnings
from sklearn.model_selection import train_test_split

def get_rmse(Y1, Y2):
 mse = np.mean(np.power(Y1-Y2,2))
 rmse = sqrt(mse)
 return rmse

def evaluate_ts_model(Y, X, order, trsz):
 warnings.simplefilter('ignore', np.RankWarning)
 warnings.simplefilter('ignore', np.ComplexWarning)
 warnings.filterwarnings("ignore")

 train_size = int(len(X) * trsz)
 test_size = int(len(X) - train_size)
 Xtr, Xtt = X[test_size:], X[0:test_size]
 Ytr, Ytt = Y[test_size:], Y[0:test_size]

 #root mean square error
 rmse = -1
 Xh = [x + 0.0 for x in Xtr]
 Yh = [y + 0.0 for y in Ytr]
 Ypr = []

 try:
  theta, residual, rank, s_val, rcond = np.polyfit(Xh, Yh, order, None, True)
  my_fun = np.poly1d(theta)
  for t in reversed(range(len(Xtt))):
   #print(str(t) + " - my_fun(Xtt[t])" + str(Xtt[t]))
   Ypr.append(my_fun(Xtt[t]))
   Xh.append(Xtt[t])
   Yh.append(Ytt[t])
  rmse = get_rmse(Ytt, Ypr)
 except:
  rmse = -1
 return rmse


def evaluate_reg_model(Y, X, order, trsz):
 warnings.simplefilter('ignore', np.RankWarning)
 warnings.simplefilter('ignore', np.ComplexWarning)
 warnings.filterwarnings("ignore")

 Xtr, Xtt, Ytr, Ytt = train_test_split(X, Y, test_size=trsz, random_state=42)

 #root mean square error
 rmse = -1
 Xh = [x + 0.0 for x in Xtr]
 Yh = [y + 0.0 for y in Ytr]
 Ypr = []

 try:
  theta, residual, rank, s_val, rcond = np.polyfit(Xh, Yh, order, None, True)
  my_fun = np.poly1d(theta)
  Ypr = my_fun(Xtt)
  rmse = get_rmse(Ytt, Ypr)
 except:
  rmse = -1
 return rmse
