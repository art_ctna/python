import sys
import unicodedata


def strip_accents(text):
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError): # unicode is a default on python 3
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)


def normalize_string(text, remove_spaces=True, remove_notext=True):
    #text to string
    text = str(text)
    #remove accents
    text = strip_accents(text.lower())
    #remove spaces
    if remove_spaces:
        # [ ]+ espacio en blanco
        text = re.sub('[ ]+', '', text)
    #remove non alphanumeric characters
    if remove_notext:
        #'[^0-9a-zA-Z_-]' non alphanumeric characters
        text = re.sub('[^0-9a-zA-Z_-]', '', text)
    return text


def main(language_file, encoding, errors):
    line = language_file.readline()

    if line:
        print_line(line, encoding, errors)
        return main(language_file, encoding, errors)

def print_line(line, encoding, errors):
    next_lang = line.strip()
    next_lang = normalize_string(line,remove_spaces=False,remove_notext=False)
    raw_bytes = next_lang.encode(encoding, errors=errors)
    cooked_string = raw_bytes.decode(encoding, errors=errors)

    print(raw_bytes, "<===>", cooked_string)

#python ex23.py
#__name__ = "__main__"
#import ex23.py
#__name__ = "mifuncion.py"
__A__ = "hola mundo"

if __name__ == "__main__":
    try:
        script, input_encoding, error = sys.argv
    except Exception as error:
        print(error)
        input_encoding = "utf-8"
        error = "strict"

    languages = open("languages.txt", encoding="utf-8")

    main(languages, input_encoding, error)
