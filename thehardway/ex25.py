__words = "renamed words"
_words = "not truly privated words"

def _my_private_function(stuff):
    """This function is not truly private function with a private variable"""
    """Used to avoid conflicts of attribute names between classes"""
    _words = stuff.split(' ')
    return _words

def __my_renamed_function(stuff):
    """This function and variable will be renamed as “_ClassName__method” """
    __words = stuff.split(' ')
    return __words

def _my_private_function(stuff):
    """This function is private function with a private variable"""
    _words = stuff.split(' ')
    return _words

def break_words(stuff):
    """This function will break up words for us."""
    words = stuff.split(' ')
    return words

def sort_words(words):
    """Sorts the words."""
    return sorted(words)

def print_first_word(words):
    """Prints the first word after popping it off."""
    word=words.pop(0)
    print(word)

def print_last_word(words):
    """Prints the last word after popping it off."""
    word=words.pop(-1)
    print(word)

def sort_sentence(sentence):
    """Takes in a full sentence and returns the sorted words."""
    words = break_words(sentence)
    return sort_words(words)

def print_first_and_last(sentence):
    words = break_words(sentence)
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """Sorts the words then prints the first and last one."""
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)
