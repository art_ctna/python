# 3.0 Introduction
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data as a dataframe
dataframe = pd.read_csv(url)
# Show first 5 rows
dataframe.head(5)

# 3.1 Creating dataframe
# Load library
import pandas as pd
# Create DataFrame
dataframe = pd.DataFrame()
# Add columns
dataframe['Name'] = ['Jacky Jackson', 'Steven Stevenson']
dataframe['Age'] = [38, 25]
dataframe['Driver'] = [True, False]
# Show DataFrame
dataframe

# Create row
new_person = pd.Series(['Molly Mooney', 40, True], index=['Name','Age','Driver'])
# Append row
dataframe.append(new_person, ignore_index=True)

#3.2 Describing data
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Show two rows
dataframe.head(2)

# Show dimensions
dataframe.shape

# Show statistics
dataframe.describe()

#3.3 Navigating DataFrames
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com//titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Select first row
dataframe.iloc[0]
# Select three rows
dataframe.iloc[1:4]
# Select three rows
dataframe.iloc[:4]

# Set index
dataframe = dataframe.set_index(dataframe['Name'])
# Show row
dataframe.loc['Allen, Miss Elisabeth Walton']


#3.4 Selecting Rows Based on Conditionals
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Show top two rows where column 'sex' is 'female'
dataframe[dataframe['Sex'] == 'female'].head(2)
# Filter rows
dataframe[(dataframe['Sex'] == 'female') & (dataframe['Age'] >= 65)]


#3.5 Replacing Values
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Replace values, show two rows
dataframe['Sex'].replace("female", "Woman").head(2)
# Replace "female" and "male with "Woman" and "Man"
dataframe['Sex'].replace(["female", "male"], ["Woman", "Man"]).head(5)

# Replace values, show two rows
dataframe.replace(1, "One").head(2)
# Replace values, show two rows
dataframe.replace(r"1st", "First", regex=True).head(2)


#3.6 Renaming Columns
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Rename column, show two rows
dataframe.rename(columns={'PClass': 'Passenger Class'}).head(2)
# Rename columns, show two rows
dataframe.rename(columns={'PClass': 'Passenger Class', 'Sex': 'Gender'}).head(2)


# Load library
import collections
# Create dictionary
column_names = collections.defaultdict(str)
# Create keys
for name in dataframe.columns:
 column_names[name]


# Show dictionary
column_names

#3.7 Finding the Minimum, Maximum, Sum, Average, and Count
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Calculate statistics
print('Maximum:', dataframe['Age'].max())
print('Minimum:', dataframe['Age'].min())
print('Mean:', dataframe['Age'].mean())
print('Sum:', dataframe['Age'].sum())
print('Count:', dataframe['Age'].count())


# Show counts
dataframe.count()

#3.8 Finding Unique Values
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Select unique values
dataframe['Sex'].unique()
# Show counts
dataframe['Sex'].value_counts()
# Show counts
dataframe['PClass'].value_counts()
# Show number of unique values
dataframe['PClass'].nunique()


#3.9 Handling Missing Values
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
## Select missing values, show two rows
dataframe[dataframe['Age'].isnull()].head(2)

# Load library
import numpy as np
# Replace values with NaN
dataframe['Sex'] = dataframe['Sex'].replace('male', np.nan)


#3.10 Deleting a Column
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Delete column
dataframe.drop('Age', axis=1).head(2)

# Drop columns
dataframe.drop(['Age', 'Sex'], axis=1).head(2)
# Drop column
dataframe.drop(dataframe.columns[1], axis=1).head(2)
# Create a new DataFrame
dataframe_name_dropped = dataframe.drop(dataframe.columns[0], axis=1)


#3.11 Deleting a Row
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Delete rows, show first two rows of output
dataframe[dataframe['Sex'] != 'male'].head(2)
# Delete two rows, show first two rows of output
dataframe.drop([0, 1],axis=0).head(2)
# Delete row, show first two rows of output
dataframe[dataframe['Name'] != 'Allison, Miss Helen Loraine'].head(2)


#3.12 Dropping Duplicate Rows
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Drop duplicates, show first two rows of output
dataframe.drop_duplicates().head(2)

# Show number of rows
print("Number Of Rows In The Original DataFrame:", len(dataframe))
print("Number Of Rows After Deduping:", len(dataframe.drop_duplicates()))

# Drop duplicates
dataframe.drop_duplicates(subset=['Sex'])
# Drop duplicates
dataframe.drop_duplicates(subset=['Sex'], keep='last')


#3.13 Grouping Rows by Values
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Group rows by the values of the column 'Sex', calculate mean
# of each group
dataframe.groupby('Sex').mean()
# Group rows
dataframe.groupby('Sex')

# Group rows, count rows
dataframe.groupby('Survived')['Name'].count()

# Group rows, calculate mean
dataframe.groupby(['Sex','Survived'])['Age'].mean()


#3.14 Grouping Rows by Time
# Load libraries
import pandas as pd
import numpy as np
# Create date range
time_index = pd.date_range('06/06/2017', periods=100000, freq='30S')
# Create DataFrame
dataframe = pd.DataFrame(index=time_index)
# Create column of random values
dataframe['Sale_Amount'] = np.random.randint(1, 10, 100000)
# Group rows by week, calculate sum per week
dataframe.resample('W').sum()

# Show three rows
dataframe.head(3)
# Group by two weeks, calculate mean
dataframe.resample('2W').mean()
# Group by month, count rows
dataframe.resample('M').count()
# Group by month, count rows
dataframe.resample('M', label='left').count()


#3.15 Looping Over a Column
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Print first two names uppercased
for name in dataframe['Name'][0:2]:
 print(name.upper())

[name.upper() for name in dataframe['Name'][0:2]]


#3.16 Applying a Function Over All Elements in a Column
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Create function
def uppercase(x):
 return x.upper()


# Apply function, show two rows
dataframe['Name'].apply(uppercase)[0:2]



#3.17 Applying a Function to Groups
# Load library
import pandas as pd
# Create URL
url = 'https://tinyurl.com/titanic-csv'
# Load data
dataframe = pd.read_csv(url)
# Group rows, apply function to groups
dataframe.groupby('Sex').apply(lambda x: x.count())


#3.18 Concatenating DataFrames
# Load library
import pandas as pd
# Create DataFrame
data_a = {'id': ['1', '2', '3'],
'first': ['Alex', 'Amy', 'Allen'],
'last': ['Anderson', 'Ackerman', 'Ali']}
dataframe_a = pd.DataFrame(data_a, columns = ['id', 'first', 'last'])
# Create DataFrame
data_b = {'id': ['4', '5', '6'],
'first': ['Billy', 'Brian', 'Bran'],
'last': ['Bonder', 'Black', 'Balwner']}
dataframe_b = pd.DataFrame(data_b, columns = ['id', 'first', 'last'])
# Concatenate DataFrames by rows
pd.concat([dataframe_a, dataframe_b], axis=0)

# Concatenate DataFrames by columns
pd.concat([dataframe_a, dataframe_b], axis=1)
# Create row
row = pd.Series([10, 'Chris', 'Chillon'], index=['id', 'first', 'last'])
# Append row
dataframe_a.append(row, ignore_index=True)

#3.19 Merging DataFrames
# Load library
import pandas as pd
# Create DataFrame
employee_data = {'employee_id': ['1', '2', '3', '4'],
'name': ['Amy Jones', 'Allen Keys', 'Alice Bees',
'Tim Horton']}
dataframe_employees = pd.DataFrame(employee_data, columns = ['employee_id',
'name'])
# Create DataFrame
sales_data = {'employee_id': ['3', '4', '5', '6'],
'total_sales': [23456, 2512, 2345, 1455]}
dataframe_sales = pd.DataFrame(sales_data, columns = ['employee_id',
'total_sales'])
# Merge DataFrames
pd.merge(dataframe_employees, dataframe_sales, on='employee_id')
# Merge DataFrames
pd.merge(dataframe_employees, dataframe_sales, on='employee_id', how='outer')

# Merge DataFrames
pd.merge(dataframe_employees, dataframe_sales, on='employee_id', how='left')

# Merge DataFrames
pd.merge(dataframe_employees,
dataframe_sales,
left_on='employee_id',
right_on='employee_id')
